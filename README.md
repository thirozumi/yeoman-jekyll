yeoman-jekyll
===

## Synopsis

This project is based on Yeoman + Jekyll + Grunt.

## Dependencies

- [Ruby 1.9+](https://www.ruby-lang.org/en/)
- [RubyGems](http://rubygems.org/)
- [Bundler](http://bundler.io/)
- [Yeoman](http://yeoman.io/)
- [Node.js](http://nodejs.org/)
- [Grunt](http://gruntjs.com/)
- [generator-jekyllrb](https://github.com/robwierzbowski/generator-jekyllrb)

## Install

```
$ bundle install
$ bower install && npm install
```

## Documentation

See [Jekyll](http://jekyllrb.com/) documentaion.

## Development

You can start built-in web server by 'serve' task.

`$ grunt serve`

and then started to connect web server, you'll can see at `http://127.0.0.1:9000`.

## Compile

`$ grunt`

## Deploy

`$ grunt deploy`

and you'll commit to 'dist' repository on /dist directory.
