---
layout: post
title:  "Yo Jekyll!"
categories: jekyll update
---

# 'Allo, 'Allo!

You now have

- Jekyll
- Html5 Boilerplate based templates
- Sass and Compass
- kramdown markdown parser

installed.

**Enjoy coding!**
